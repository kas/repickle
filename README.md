# Repickle

⚠️  This codebase is unmaintained, please use e.g. [dasel](https://github.com/TomWright/dasel "Select, put and delete data from JSON, TOML, YAML, XML and CSV files with a single tool. Supports conversion between formats and can be used as a Go package.") instead.

Convert between Python serialization formats, possibly pretty printing the output.

## Supported formats

* Internal
  * [JSON](https://docs.python.org/3.5/library/json.html)
  * [Marshal](https://docs.python.org/3.5/library/marshal.html)
  * [Pickle](https://docs.python.org/3.5/library/pickle.html)
* External
  * [AXON](http://intellimath.bitbucket.org/axon/)
  * [MessagePack](https://msgpack.org/)
  * [TOML](https://github.com/toml-lang/toml)
  * [YAML](http://yaml.org/)

## Similar projects

* [Dasel](https://github.com/TomWright/dasel) (Go)
* [Remarshal](https://github.com/dbohdan/remarshal) (Go)
